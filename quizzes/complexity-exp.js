// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger
// SPDX-License-Identifier: CC-BY-SA-4.0

quizComplexityExp = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Multiply fixed times for exponents.",
        "level1":  "Excellent!",     // 80-100%
        "level2":  "Maybe ask for help?", // 60-79%
        "level3":  "Maybe ask for help?", // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Maybe ask for help?"  // 0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about the result of naive_exp, where “^” indicates a power calculation.",
            "a": [
                {"option": "naive_exp(op1,0) = 1", "correct": true},
                {"option": "naive_exp(op1,1) = op1", "correct": true},
                {"option": "naive_exp(op1,5) = op1*op1*op1*op1*op1 = op1^5", "correct": true},
                {"option": "naive_exp(op1,op2) = op1^op2", "correct": true}
            ],
            "correct": "<p><span>Very good!  This algorithm indeed computes powers for given exponents.</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        },
	{
            "q": "Select correct statements about multiplications in naive_exp.",
            "a": [
                {"option": "naive_exp(op1,0) does not require multiplications.", "correct": true},
                {"option": "3^5 = 3*3*3*3*3.  Thus, we can multiply 5-1 = 4 times for the power of 5.", "correct": true},
                {"option": "The first argument does not influence the number of multiplications.", "correct": true},
                {"option": "The algorithm needs op2-1 multiplications, i.e., the number of multiplications is linear in the second argument.", "correct": true}
            ],
            "correct": "<p><span>Very good!  Now, how many plus operations are necessary for a multiplication?</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        },
	{
            "q": "Select correct statements about plus operations in naive_exp.",
            "a": [
                {"option": "naive_exp(op1,0) does not require plus operations.", "correct": true},
                {"option": "3^5 = 3*3*3*3*3 = (3+3+3)*3*3*3 = (9+9+9)*3*3 = (27+27+27)*3 = 81+81+81.  Thus, we use (3-1)*(5-1) = 2*4 = 8 add operations.", "correct": true},
                {"option": "The algorithm needs (op1-1)*(op2-1) plus operations.", "correct": true}
            ],
            "correct": "<p><span>Very good!  The complexity is O(op1*op2).</span></p>",
            "incorrect": "<p>Do you need help?  All statements are correct.  Which one confuses you?</p>" // no comma here
        }
    ]
};
